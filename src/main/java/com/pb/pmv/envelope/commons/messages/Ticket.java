package com.pb.pmv.envelope.commons.messages;

import java.util.Date;

/**
 * Created by it200190pmv on 10/29/15.
 */
public class Ticket {
    private final long id;
    private final Date date;
    private final long type;
    private final String text;
    private final String statys;

    public Ticket() {
        this.id = 0;
        this.date = new Date();
        this.type = 0;
        this.text = "";
        this.statys = "";
    }

    public Ticket(long id, Date date, long type, String text, String statys) {
        this.id = id;
        this.date = date;
        this.type = type;
        this.text = text;
        this.statys = statys;
    }

    public long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public long getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public String getStatys() {
        return statys;
    }
}
