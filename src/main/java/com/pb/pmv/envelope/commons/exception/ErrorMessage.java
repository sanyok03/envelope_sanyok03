/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.pmv.envelope.commons.exception;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Для форамта ошибок в контроллерах
 *
 * @author it200190pmv
 */
@XmlRootElement(name = "error")
public class ErrorMessage {

    private String cause;
    private String code;
    private String stackTrace;
    private String text;

    public String getCause() {
        return cause;
    }
    
    @XmlElement
    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getCode() {
        return code;
    }
    
    @XmlElement
    public void setCode(String code) {
        this.code = code;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    @XmlElement
    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    public String getText() {
        return text;
    }

    @XmlElement
    public void setText(String text) {
        this.text = text;
    }
}
