package com.pb.pmv.envelope.commons.exception;

/**
 * Created by it200190pmv on 29/10/15.
 */
public class AuthException extends Exception {
    private static final long serialVersionUID = -3526794570620294540L;

    public AuthException() {
        super("Ошибка работы с сервисом авторизации");
    }

    public AuthException(String message) {
        super(message);
    }

    public AuthException(String message, String sid) {
        super(message + ": " + sid);
    }

    public AuthException(Throwable cause) {
        super(cause);
    }
}
