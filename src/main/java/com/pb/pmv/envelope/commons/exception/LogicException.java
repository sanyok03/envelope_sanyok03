/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.pmv.envelope.commons.exception;

/**
 *
 * @author it200190pmv
 */
public class LogicException  extends Exception{

    private String errorText;
    private String errorCode;
    
    public LogicException() {
        super();
    }

    public LogicException(String errorText, String errorCode) {
        super(errorText);
        this.errorText = errorText;
        this.errorCode = errorCode;
    }

 

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    
    
}
