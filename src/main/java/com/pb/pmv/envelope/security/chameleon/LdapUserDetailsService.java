package com.pb.pmv.envelope.security.chameleon;

import com.pb.chameleon.ejb_api.session_manager.UserLogin;
import com.pb.pmv.envelope.commons.exception.AuthException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * don't used yet
 * Created by it200190pmv on 10/13/15.
 */
@Component
public class LdapUserDetailsService implements AuthenticationUserDetailsService {
    private static final Logger LOGGER = LoggerFactory.getLogger("services");

    private static final String SUBSYS = "CXP";

    @Autowired
    private ISessionService sessionService;


    public UserDetails loadUserDetails(Authentication token) throws UsernameNotFoundException {
        ChameleonUserDetail chameleonUserDetail = new ChameleonUserDetail();
        if (token == null || token.getPrincipal() == null) {
            return chameleonUserDetail;
        }
        String sid = (String) token.getPrincipal();
        try {
            UserLogin userLogin = sessionService.getUserLogin(sid);
            chameleonUserDetail.setUser(userLogin);
            List<String> roles = sessionService.getRoles(sid, SUBSYS);
            List<GrantedAuthority> lstRoles = new ArrayList<>();
            LOGGER.info(String.format("Получены роли в Промине: %s", Arrays.toString(roles.toArray(new String[]{}))));
            for (String role : roles) {
                ChameleonRole chameleonRole = new ChameleonRole();
                chameleonRole.setRole(role);
                chameleonRole.setSubSys(SUBSYS);
                lstRoles.add(chameleonRole);
            }
            chameleonUserDetail.setAuthorities(lstRoles);
        } catch (AuthException e) {
            LOGGER.warn("LdapUserDetailsService: ошибка при получнии ролей в Промине.", e);
        }
        return chameleonUserDetail;
    }
}
