package com.pb.pmv.envelope.security;

import com.pb.pmv.envelope.security.chameleon.ISessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.annotation.PostConstruct;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * Created by it200190pmv on 10/9/15.
 */
public class LoginFilter extends AbstractAuthenticationProcessingFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger("auth");

    private String authorizeUrl;
    private String clientId;
    private String scope;
    private String state;
    private String responce_type;

    public ISessionService sessionService;
    private SpAuthentification spAuthentification;

    public LoginFilter() {
        super("/authorize");
//        LOGGER.info("LoginFilter С");
        SavedRequestAwareAuthenticationSuccessHandler saash = (SavedRequestAwareAuthenticationSuccessHandler) getSuccessHandler();
        saash.setTargetUrlParameter("origin_uri");
        saash.setDefaultTargetUrl("/create/konvert.html");
    }

    @PostConstruct
    private void init() {
//        LOGGER.info("LoginFilter PC");
//        printParams();
    }

    private void printParams() {
        LOGGER.info(String.format("authorizeUrl=%s", authorizeUrl));
        LOGGER.info(String.format("clientId=%s", clientId));
        LOGGER.info(String.format("scope=%s", scope));
        LOGGER.info(String.format("state=%s", state));
        LOGGER.info(String.format("responce_type=%s", responce_type));
    }

    @Override
    public void setAuthenticationDetailsSource(AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource) {
        super.setAuthenticationDetailsSource(authenticationDetailsSource);
    }

    @Override
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }

    public void setUserAuthorizationUrl(String userAuthorizationUrl) {
//        LOGGER.info("setUserAuthorizationUrl");
        printParams();
        authorizeUrl = new StringBuilder(userAuthorizationUrl).append("?client_id=").append(clientId)
                .append("&scope=").append(scope).append("&response_type=").append(responce_type).append("&state=").append(state)
                .append("&redirect_uri=").toString();
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
//        LOGGER.info("attemptAuthentication");
        Authentication auth = null;
        return auth;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
//        LOGGER.info("LoginFilter.doFilter");
//        printParams();
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        Authentication auth = spAuthentification.loadFromVerifiedCookie(request, response);
        String code = request.getParameter("code");
        if (auth == null && code == null) {
            String originUri = request.getParameter("origin_uri");
            String redirectURL = URLEncoder.encode(request.getRequestURL() + (originUri == null ? "" : "?origin_uri=" + originUri), Constants.ENCODING);
//            LOGGER.info("LoginFilter.doFilter redirect to " + authorizeUrl + redirectURL);
            response.sendRedirect(authorizeUrl + redirectURL);
            return;
        } else if (auth == null) {
//            LOGGER.info("LoginFilter.doFilter auth==null auth = authentificate(request, response)");
            auth = authentificate(request, response);
        }
//        LOGGER.info("auth: " + auth + "auth.isAuthenticated=" + auth.isAuthenticated());
//        LOGGER.info("request RequestURI=" + request.getRequestURI());
//        LOGGER.info(request.getQueryString() + ", " + request.getServletPath() + ", " + request.getContextPath() + ", " + request.getAuthType());
        SecurityContextHolder.getContext().setAuthentication(auth);
        super.doFilter(request, response, chain);
    }

    private Authentication authentificate(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Authentication auth = null;
        Token token = spAuthentification.getToken(request);
        if (token != null) {
            User user = spAuthentification.getUser(token);
            if (user != null) {
//                LOGGER.info("LoginFilter call spAuthentification.saveVerifiedCookie");
                spAuthentification.saveVerifiedCookie(request, response, user, token);
                auth = new UsernamePasswordAuthenticationToken(user, token, null);
            }
        }

        if (auth == null) {
//            LOGGER.info("LoginFilter.authentificate redirect to " + request.getContextPath() + "/error/authentification.html");
            response.sendRedirect(request.getContextPath() + "/error/authentification.html");
        }

        return auth;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

//    public void setAuthorizeUrl(String authorizeUrl) {
//        this.authorizeUrl = authorizeUrl;
//    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setResponce_type(String responce_type) {
        this.responce_type = responce_type;
    }

    public void setSessionService(ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void setSpAuthentification(SpAuthentification spAuthentification) {
        LOGGER.info(spAuthentification == null ? "setSpAuthentification is Null" : "setSpAuthentification is normal");
        this.spAuthentification = spAuthentification;
        LOGGER.info("setSpAuthentification");
    }
}
