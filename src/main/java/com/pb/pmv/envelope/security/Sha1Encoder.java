package com.pb.pmv.envelope.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by it200190pmv on 10/12/15.
 */
public final class Sha1Encoder {
    private static final char[] KDIGITS = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    };

    private Sha1Encoder() {
    }

    public static String encode(String string) {
        try {
            if (string == null) {
                return "";
            }
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            sha.reset();
            sha.update(string.getBytes(Constants.CHARSET), 0, string.length());
            return bytesToHexStr(sha.digest());
        } catch (NoSuchAlgorithmException ex) {
            return string;
        }
    }

    private static String bytesToHexStr(byte[] raw) {
        char[] hex = new char[raw.length * 2];
        for (int i = 0; i < raw.length; i++) {
            int value = raw[i] & 0xFF;
            hex[i * 2] = KDIGITS[value >>> 4];
            hex[i * 2 + 1] = KDIGITS[value & 0x0F];
        }
        return new String(hex);
    }
}
