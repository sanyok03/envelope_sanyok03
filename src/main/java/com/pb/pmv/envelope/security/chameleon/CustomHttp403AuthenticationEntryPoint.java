package com.pb.pmv.envelope.security.chameleon;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.pb.pmv.envelope.commons.exception.ErrorMessage;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by it200190pmv on 10/12/15.
 */
public class CustomHttp403AuthenticationEntryPoint implements AuthenticationEntryPoint {

    private ObjectMapper objectMapper;

    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setCode("403");
        errorMessage.setCause(e.getCause().toString());
        errorMessage.setText(e.getMessage());

        byte[] bytes = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsBytes(errorMessage);
        httpServletResponse.setContentType("application/json");
        httpServletResponse.getOutputStream().write(bytes);
    }
}
