package com.pb.pmv.envelope.security.chameleon;

import com.pb.chameleon.ejb_api.session_manager.UserLogin;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by it200190pmv on 10/13/15.
 */
public class ChameleonUserDetail implements UserDetails {
    Collection<GrantedAuthority> authorities = new ArrayList<>();
    UserLogin user = null;

    public UserLogin getUser() {
        return user;
    }

    public void setUser(UserLogin user) {
        this.user = user;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public String getPassword() {
        return user.getLogin();
    }

    public String getUsername() {
        return user.getLogin();
    }

    public boolean isAccountNonExpired() {
        return user != null;
    }

    public boolean isAccountNonLocked() {
        return user != null;
    }

    public boolean isCredentialsNonExpired() {
        return user != null;
    }

    public boolean isEnabled() {
        return user != null;
    }
}
