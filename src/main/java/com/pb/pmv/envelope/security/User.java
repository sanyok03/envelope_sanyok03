package com.pb.pmv.envelope.security;

import java.io.Serializable;

/**
 * Created by it200190pmv on 10/12/15.
 */
public class User implements Serializable {

    private String login;
    private String auth;

    public User(String login, String auth) {
        this.login = login;
        this.auth = auth;
    }

    public String getLogin() {
        return login;
    }

    public String getAuth() {
        return auth;
    }
}
