package com.pb.pmv.envelope.security;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Created by it200190pmv on 10/12/15.
 */
public class Token implements Serializable {

    private String token;
    private long expiring;

    public Token(String token, int ttls) {
        this.token = token;
        this.expiring = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(ttls);
    }

    public Token(String token, long expiring) {
        this.token = token;
        this.expiring = expiring;
    }

    public String getToken() {
        return token;
    }

    public long getExpiring() {
        return expiring;
    }
}