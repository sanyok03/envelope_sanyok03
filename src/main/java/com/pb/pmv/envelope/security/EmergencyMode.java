package com.pb.pmv.envelope.security;

/**
 * Created by it200190pmv on 10/12/15.
 */
public enum EmergencyMode {

    ON, OFF
}
