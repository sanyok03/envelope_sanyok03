package com.pb.pmv.envelope.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * Created by it200190pmv on 10/12/15.
 */
public class NotAuthenticatedEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        response.sendRedirect(request.getContextPath() + "/authorize?origin_uri=" + URLEncoder.encode(request.getRequestURL().toString(), Constants.ENCODING));
    }
}
