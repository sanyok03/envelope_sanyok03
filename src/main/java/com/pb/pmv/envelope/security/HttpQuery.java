package com.pb.pmv.envelope.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by it200190pmv on 10/12/15.
 */
public class HttpQuery {

    private static Logger LOGGER = LoggerFactory.getLogger("commons");

    private static final String REAL_IP_HEADER = "X-Real-IP";

    private HttpQuery() {
    }

    public static String getServerDomain(HttpServletRequest request) {
        try {
            String url = new URL(request.getRequestURL().toString()).getHost().trim();
            LOGGER.info("getServerDomain url="+url);
            return url;
        } catch (MalformedURLException ex) {
            return null;
        }
    }

    public static String getClientHost(HttpServletRequest request) {
        LOGGER.info("getClientHost");
        String clientHost = request.getRemoteAddr();
        if (request.getHeader(REAL_IP_HEADER) != null) {
            clientHost = request.getHeader(REAL_IP_HEADER);
        }
        LOGGER.info("return " + clientHost == null ? null : clientHost.trim());
        return clientHost == null ? null : clientHost.trim();
    }

    public static Cookie findCookie(HttpServletRequest request, String cookieName) {
        LOGGER.info("findCookie");
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equalsIgnoreCase(cookieName)) {
                    LOGGER.info("cookie={" + cookie.getValue() + "}");
                    return cookie;
                }
            }
        }
        return null;
    }

    public static Cookie createCookie(String name, String value, String domainName, int expiry) {
        LOGGER.info("createCookie");
        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");
        cookie.setDomain(domainName);
        cookie.setMaxAge(expiry);
        return cookie;
    }
}
