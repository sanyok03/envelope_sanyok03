package com.pb.pmv.envelope.security.chameleon;

import com.pb.Chameleon.Session.ManagerSystem.SessionManager;
import com.pb.Chameleon.util.ChameleonApiTools;
import com.pb.Chameleon.util.DataType;
import com.pb.chameleon.ejb_api.session.LoginException;
import com.pb.chameleon.ejb_api.session_manager.UserLogin;
import com.pb.pmv.envelope.commons.exception.AuthException;
import com.pb.pmv.envelope.security.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by it200190pmv on 10/12/15.
 */

@Component
public class SessionService implements ISessionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SessionService.class);
    private static final Charset CHARSET = Constants.CHARSET;
    private SessionManager sessionManager;
    /**
     * Время жизни сессии, в минутах
     */
    private final int sessionTTL = 45;
    /**
     * Адрес сервера Проминя
     */
    @Value("${chameleon.SessionService.url}")
    private String url;
    /**
     * Сервисный логин
     */
    @Value("${chameleon.SessionService.login}")
    private String login;
    /**
     * Система авторизации сервисного логина
     */
    @Value("${chameleon.SessionService.authSys}")
    private String authSys;
    /**
     * Пароль сервисного логина
     */
    @Value("${chameleon.SessionService.password}")
    private String password;
    /**
     * Текущая сессия Проминя
     */
    private String currentSession;
    /**
     * Дата, когда нужно обновить сессию
     */
    private Long sessionExpiry;
    /**
     * Блокировка для пересоздания сессии
     */
    private Lock sessionCreationLock = new ReentrantLock();

    public SessionService() {
    }

    /**
     * Конструктор
     *
     * @param url      Адрес сервера Проминя
     * @param login    Сервисный логин
     * @param authSys  Система авторизации сервисного логина
     * @param password Пароль сервисного логина
     */
    public SessionService(String url,
                          String login,
                          String authSys,
                          String password,
                          int connectTimeout,
                          int readTimeout) {
        this.url = url;
        this.login = login;
        this.authSys = authSys;
        this.password = password;
        this.currentSession = null;
    }

    @PostConstruct
    private final void init() {
        ChameleonApiTools.setDataType(DataType.JSON);
        sessionExpiry = System.currentTimeMillis();
        try {
            sessionManager = SessionManager.getInstance(url);
        } catch (LoginException ex) {
            LOGGER.error(ex.getMessage(), ex);
        } catch (RemoteException ex) {
            LOGGER.error(ex.getMessage(), ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        LOGGER.info("Поднятие бина авторизации");
        LOGGER.info("ReadTimeout: " + ChameleonApiTools.getReadTimeout() + " ms");
        LOGGER.info("ConnectTimeout: " + ChameleonApiTools.getConnectionTimeout() + " ms");
        LOGGER.info("URL: " + url);
        LOGGER.info("LOGIN: " + login);
        LOGGER.info("AUTHSYS: " + authSys);
        LOGGER.info("PASS: " + password == null ? "null" : "XXX");
    }

    public String getUserProperty(String sid, String name) throws RemoteException {
        return sessionManager.getSessionAttributes(sid).getProperty(name);
    }

    /**
     * Получение сервисной сессии Проминя
     *
     * @return String
     * @throws Exception
     */
    public String getSession() throws AuthException {
        if (!isSessionUsable()) {
            try {
                recreateSession();
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                throw new AuthException(e);
            }
        }
        return currentSession;
    }

    public String getGlobalParam(String sid, String param) throws AuthException {
        try {
            return this.getSessionManager().getGlobalParameter(sid, param);
        } catch (RemoteException e) {
            LOGGER.error(e.getMessage(), e);
            throw new AuthException(e);
        }
    }

    public String getSessionAttribute(String sid, String attr) throws AuthException {
        try {
            return this.getSessionManager().getSessionAttributes(sid).getProperty(attr);
        } catch (RemoteException e) {
            LOGGER.error(e.getMessage(), e);
            throw new AuthException(e);
        }
    }


    public String getSysAuthAttr(String sid, String attrName) throws AuthException {
        try {
            return sessionManager.getSysAuthProperties(sid).getProperty(attrName);
        } catch (RemoteException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new AuthException("Ошибка получения атрибутов сессии", sid);
        }
    }

    public UserLogin getUserLogin(String sid) throws AuthException {
        UserLogin userLogin = null;
        try {
            userLogin = this.getSessionManager().getUserLogin(sid, true);
            if (userLogin == null) {
                throw new AuthException("Ошибка подтверждения сессии", sid);
            }
            return userLogin;
        } catch (RemoteException e) {
            LOGGER.error(e.getMessage(), e);
            throw new AuthException(e);
        }
    }

    public String getUserBranch(String sid) throws AuthException {
        if (getGlobalParam(sid, "brnm") != null && !getGlobalParam(sid, "brnm").isEmpty()) {
            return getGlobalParam(sid, "brnm").toUpperCase();
        } else if (getSessionAttribute(sid, "ROTATION_BRNM") != null && !getSessionAttribute(sid, "ROTATION_BRNM").isEmpty()) {
            return getSessionAttribute(sid, "ROTATION_BRNM").toUpperCase();
        } else {
            return getSessionAttribute(sid, "ECA_BRNM").toUpperCase();
        }
    }

    private SessionManager getSessionManager() throws AuthException {
        return sessionManager;
    }

    /**
     * Получение списка ролей по сессии ПК Проминь
     *
     * @param sid    Сессия ПК Проминь
     * @param subSys
     * @return Набор ролей
     */
    public List<String> getRoles(String sid, String subSys) throws AuthException {
        List<String> lstRoles = new ArrayList<String>();
        try {
            Set<Object> lstKeys = this.getSessionManager().getRoles(sid, subSys).keySet();
            for (Object key : lstKeys) {
                lstRoles.add(subSys + ":" + key.toString());
            }
        } catch (RemoteException e) {
            LOGGER.error("Ошибка получения ролей", e);
            throw new AuthException(e);
        }
        return lstRoles;
    }


    /**
     * * Закрытие сессии
     *
     * @param sid
     * @throws AuthException
     */
    public void dropSession(String sid) throws AuthException {
        try {
            getSessionManager().dropSession(sid);
        } catch (RemoteException e) {
            LOGGER.error("Ошибка закрытия сессии", e);
            throw new AuthException(e);
        }

    }

    /**
     * Проверка не необходимость пересоздания сессии
     *
     * @return boolean
     */
    private boolean isSessionUsable() {
        return sessionExpiry > System.currentTimeMillis();
    }

    /**
     * Пересоздает сессию, если нужно.
     *
     * @throws Exception
     */
    private void recreateSession() throws Exception {
        try {
            sessionCreationLock.lock();
            if (isSessionUsable())
                return;
            openNewSession();
        } finally {
            sessionCreationLock.unlock();
        }
    }

    /**
     * Открывает новую сессию.
     *
     * @throws Exception
     */
    private void openNewSession() throws Exception {
        try {
            currentSession = this.getSessionManager().createSession(login, authSys, password, "", "");
            Calendar cl = Calendar.getInstance();
            cl.add(Calendar.MINUTE, sessionTTL);
            sessionExpiry = cl.getTimeInMillis();
        } catch (LoginException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new Exception("Configuration error or login locked", ex);
        } catch (RemoteException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new Exception("Exception from Promin server", ex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new Exception("Exception from Promin server", ex);
        }
    }
}
