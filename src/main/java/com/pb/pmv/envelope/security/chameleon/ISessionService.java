package com.pb.pmv.envelope.security.chameleon;

import com.pb.chameleon.ejb_api.session_manager.UserLogin;
import com.pb.pmv.envelope.commons.exception.AuthException;

import java.rmi.RemoteException;
import java.util.List;

/**
 * Created by it200190pmv on 10/12/15.
 */
public interface ISessionService {
    String getGlobalParam(String sid, String param) throws AuthException;

    String getSessionAttribute(String sid, String attr) throws AuthException;

    String getSysAuthAttr(String sid, String attrName) throws AuthException;

    UserLogin getUserLogin(String sid) throws AuthException;

    String getUserBranch(String sid) throws AuthException;

    /**
     * Получение параметра пользователя по сессии
     *
     * @param sid  Сессия
     * @param name Имя параметра
     * @return
     * @throws java.rmi.RemoteException
     */
    String getUserProperty(String sid, String name) throws RemoteException;

    /**
     * Получение списка ролей по сессии ПК Проминь
     *
     * @param sid    Сессия ПК Проминь
     * @param subSys
     * @return Набор ролей
     */
    List<String> getRoles(String sid, String subSys) throws AuthException;

    /**
     * Закрытие сессии
     *
     * @param sid
     * @throws AuthException
     */
    void dropSession(String sid) throws AuthException;

    /**
     * Получение сессии ПК Проминь
     *
     * @return Сессия
     */
    String getSession() throws AuthException;
}
