package com.pb.pmv.envelope.security;

import java.nio.charset.Charset;

/**
 * Created by it200190pmv on 10/12/15.
 */
public interface Constants {
    String ENCODING = "UTF-8";
    Charset CHARSET = Charset.forName(ENCODING);
}
