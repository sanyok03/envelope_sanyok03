package com.pb.pmv.envelope.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pb.pmv.envelope.security.chameleon.ISessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by it200190pmv on 10/12/15.
 */
@Component
public class SpAuthentification {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpAuthentification.class);

    private static final int VERIFIED_COOKIE_TTLS = 2700; //45 минут, 86400 - Сутки
    public static final String VERIFIED_COOKIE_NAME = "WebEnvlp.verified";
    private static final String SESSION_ID_COOKIE = "SessionID";
    private static final String EMERGENCY_TOKEN = "EMERGENCY_TOKEN";
    private static final String FAMILY_PROPERTY = "PEOP_Family";
    private static final String NAME_PROPERTY = "PEOP_Name";
    private static final String MNAME_PROPERTY = "PEOP_SurName";

    @Value("${security.SpAuthentification.clientId}")
    private String clientId;
    @Value("${security.SpAuthentification.clientSecret}")
    private String clientSecret;
    @Value("${security.SpAuthentification.getTokenUrl}")
    private String getTokenUrl;
    @Value("${security.SpAuthentification.getUserUrl}")
    private String getUserUrl;
    @Value("${security.SpAuthentification.LogoutUrl}")
    private String logoutUrl;
    @Value("${security.SpAuthentification.LogoutFallbackUrl}")
    private String logoutFallbackUrl;
    @Value("${security.SpAuthentification.verifiedConfidenceTTLs}")
    private int verifiedConfidenceTTLs;
    @Value("${security.SpAuthentification.verifiedSalt}")
    private String verifiedSalt;
    @Value("${security.SpAuthentification.RequestTimeout}")
    private int requestTimeout;
    private String authorizeUrl;

//    @Autowired
    private ObjectMapper objectMapper;
    private ISessionService sessionService;

    @PostConstruct
    private void init() {
        LOGGER.info("SpAuthentification was created");
        LOGGER.info("authorizeUrl="+authorizeUrl);
    }

    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    private static void removeVerifiedCookie(HttpServletRequest request, HttpServletResponse response) {
        response.addCookie(HttpQuery.createCookie(VERIFIED_COOKIE_NAME, "REMOVED", HttpQuery.getServerDomain(request), 0));
        response.addCookie(HttpQuery.createCookie(SESSION_ID_COOKIE, "REMOVED", HttpQuery.getServerDomain(request), 0));
        request.getSession().removeAttribute("roles");
    }

    private static String buildRedirectUrl(HttpServletRequest request) throws UnsupportedEncodingException {
        String originUri = request.getParameter("origin_uri");
        return URLEncoder.encode(request.getRequestURL() + (originUri == null ? "" : "?origin_uri=" + originUri), Constants.ENCODING);
    }


    @Autowired
    public void setUserAuthorizationUrl(@Value("${security.SpAuthentification.authorizeUrl}") String userAuthorizationUrl) {
        authorizeUrl = new StringBuilder(userAuthorizationUrl).append("?client_id=").append(clientId)
                .append("&scope=read&response_type=code&state=enter")
                .append("&redirect_uri=").toString();
    }

    public void writeRedirectJson(HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        ServletOutputStream os = response.getOutputStream();
        os.write(objectMapper.writeValueAsBytes(Collections.singletonMap("notAuthorize", true)));
        os.flush();
        os.close();
    }

    public void redirectToAuthorizer(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String redirectUri = authorizeUrl + buildRedirectUrl(request);
        response.sendRedirect(redirectUri);
    }

    public Token getToken(HttpServletRequest request) {
        try {
            String authCode = request.getParameter("code");
            String redirectUrl = buildRedirectUrl(request);
            String tokenRequest = "grant_type=authorization_code&code=" + authCode + "&redirect_uri=" + redirectUrl;
            LOGGER.info("SpAuthentification.tokenRequest=" + tokenRequest);
            Map map = new RestTemplateLight(new GetTokenHttpRequestFactory(clientId, clientSecret, requestTimeout)).postForObject(getTokenUrl, tokenRequest, Map.class);
            return new Token((String) map.get("access_token"), verifiedConfidenceTTLs);
        } catch (Exception ex) {
            LOGGER.warn("", ex);
            return null;
        }
    }

    public User getUser(Token token) {
        try {
            Map map = new RestTemplateLight(new TokenHttpRequestFactory(token.getToken(), requestTimeout)).getForObject(getUserUrl, Map.class);
            return new User((String) map.get("username"), (String) map.get("authsystem"));
        } catch (Exception ex) {
            LOGGER.warn("", ex);
            return null;
        }
    }

    public String logout(HttpServletRequest request, HttpServletResponse response, Token token) {
        removeVerifiedCookie(request, response);
        try {
            String fullLogoutURL = new StringBuilder(logoutUrl).append("?client_id=").append(clientId)
                    .append("&scope=read&start_uri=").append(URLEncoder.encode(logoutFallbackUrl,
                            Constants.ENCODING)).toString();
            Map map = new RestTemplateLight(new TokenHttpRequestFactory(token.getToken(), requestTimeout))
                    .getForObject(fullLogoutURL, Map.class);
//                return (String) map.get("redirect_uri");
            return logoutFallbackUrl;
        } catch (Exception ex) {
            LOGGER.warn("", ex);
        }

        return logoutFallbackUrl;
    }

    public Authentication loadFromVerifiedCookie(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = null;

        Cookie verifiedCookie = HttpQuery.findCookie(request, VERIFIED_COOKIE_NAME);
        String[] value = verifiedCookie != null ? verifiedCookie.getValue().split("\\*") : new String[0];

        if (value.length == 5 && createVerifiedCrc(new StringBuilder(value[0]).append('*').append(value[1])
                .append('*').append(value[2]).append('*').append(value[3]).toString()) == Integer.parseInt(value[4])) {
            long expiring = Long.parseLong(value[3]);
            if (expiring > System.currentTimeMillis()) {
                auth = new UsernamePasswordAuthenticationToken(new User(value[0], value[1]), new Token(value[2], expiring), null);
                LOGGER.info("expiring > System.currentTimeMillis() addCookie={" +
                        VERIFIED_COOKIE_NAME + ", " +
                        verifiedCookie.getValue() + ", " +
                        HttpQuery.getServerDomain(request) + ", " +
                        VERIFIED_COOKIE_TTLS + "}");
                response.addCookie(HttpQuery.createCookie(VERIFIED_COOKIE_NAME, verifiedCookie.getValue(), HttpQuery.getServerDomain(request), VERIFIED_COOKIE_TTLS));
            } else {
                Token token = new Token(value[2], verifiedConfidenceTTLs);
                User user = EMERGENCY_TOKEN.equals(token.getToken()) ? null : getUser(token);
                if (user != null) {
                    LOGGER.info("SpAuthentification.saveVerifiedCookie");
                    saveVerifiedCookie(request, response, user, token);
                    auth = new UsernamePasswordAuthenticationToken(user, token, null);
                } else {
                    LOGGER.info("removeVerifiedCookie");
                    removeVerifiedCookie(request, response);
                }
            }
        }

//        if (auth == null) {
////            try {
////                redirectToAuthorizer(request, response);
////            } catch (IOException e) {
////// TODO Auto-generated method stub
////                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
////            }
//            User user = null;
//            if (user != null) {
//                Token token = new Token(EMERGENCY_TOKEN, verifiedConfidenceTTLs);
//                auth = new UsernamePasswordAuthenticationToken(user, token, null);
//                saveVerifiedCookie(request, response, user, token);
//            }
//        }

        return auth;
    }

    public void saveVerifiedCookie(HttpServletRequest request, HttpServletResponse response, User user, Token token) {
        StringBuilder value = new StringBuilder(user.getLogin()).append('*').append(user.getAuth())
                .append('*').append(token.getToken()).append('*').append(token.getExpiring());
        int crc = createVerifiedCrc(value.toString());
        value.append('*').append(crc);
        LOGGER.info("addCookie={" +
                VERIFIED_COOKIE_NAME + ", " +
                value.toString() + ", " +
                HttpQuery.getServerDomain(request) + ", " +
                VERIFIED_COOKIE_TTLS + "}");
        response.addCookie(HttpQuery.createCookie(VERIFIED_COOKIE_NAME, value.toString(), HttpQuery.getServerDomain(request), VERIFIED_COOKIE_TTLS));
        LOGGER.info("addCookie={" +
                SESSION_ID_COOKIE + ", " +
                token.getToken() + ", " +
                HttpQuery.getServerDomain(request) + ", " +
                VERIFIED_COOKIE_TTLS + "}");
        response.addCookie(HttpQuery.createCookie(SESSION_ID_COOKIE, token.getToken(), HttpQuery.getServerDomain(request), VERIFIED_COOKIE_TTLS));
    }

    private String buildFIO(Token token) {
        String fio = "Выход";
        try {
            String family = sessionService.getUserProperty(token.getToken(), FAMILY_PROPERTY);
            String name = sessionService.getUserProperty(token.getToken(), NAME_PROPERTY);
            String mname = sessionService.getUserProperty(token.getToken(), MNAME_PROPERTY);
            fio = URLEncoder.encode(new StringBuilder().append(family).append(" ")
                    .append(name != null && name.length() > 0 ? name.substring(0, 1) : "")
                    .append(". ")
                    .append(mname != null && mname.length() > 0 ? mname.substring(0, 1) : "")
                    .append(".").toString(), "UTF-8");
//            fio = new StringBuilder().append(family).append("<br/>")
//                    .append(name != null && name.length() > 0 ? name.substring(0,1) : "")
//                    .append(".")
//                    .append(mname != null && mname.length() > 0 ? mname.substring(0,1) : "")
//                    .append(".").toString();
        } catch (RemoteException e) {
            LOGGER.error("Ошибка получения ФИО", e);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Ошибка предобразования ФИО", e);
        }
        return fio;
    }

    private int createVerifiedCrc(String value) {
        return Sha1Encoder.encode(verifiedSalt + value + verifiedSalt).hashCode();
    }

    private static class RestTemplateLight extends RestTemplate {

        private static List<HttpMessageConverter<?>> converters;

        static {
            converters = new LinkedList<HttpMessageConverter<?>>();
            converters.add(new StringHttpMessageConverter());
            converters.add(new MappingJackson2HttpMessageConverter());
        }

        private RestTemplateLight(ClientHttpRequestFactory requestFactory) {
//            super(converters);
            super();
            setMessageConverters(converters);
            setRequestFactory(requestFactory);
        }
    }

    private static class TokenHttpRequestFactory extends SimpleClientHttpRequestFactory {

        private final String token;

        public TokenHttpRequestFactory(String token, int requestTimeout) {
            this.token = token;
            setConnectTimeout(requestTimeout);
            setReadTimeout(requestTimeout);
        }

        @Override
        protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
            super.prepareConnection(connection, httpMethod);
            connection.setRequestProperty("Authorization", "Bearer " + token);
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        }
    }

    private static class GetTokenHttpRequestFactory extends SimpleClientHttpRequestFactory {

        private final String clientId;
        private final String secret;

        public GetTokenHttpRequestFactory(String clientId, String secret, int requestTimeout) {
            this.clientId = clientId;
            this.secret = secret;
            setConnectTimeout(requestTimeout);
            setReadTimeout(requestTimeout);
        }

        @Override
        protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
            super.prepareConnection(connection, httpMethod);
            String authorization = clientId + ":" + secret;
            byte[] encodedAuthorisation = Base64.encode(authorization.getBytes(Constants.CHARSET));
            connection.setRequestProperty("Authorization", "Basic " + new String(encodedAuthorisation, Constants.CHARSET));
            connection.setRequestProperty("Accept", "application/json, application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        }
    }

}
