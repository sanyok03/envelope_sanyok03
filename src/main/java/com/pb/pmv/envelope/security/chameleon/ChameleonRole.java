package com.pb.pmv.envelope.security.chameleon;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by it200190pmv on 10/13/15.
 */
public class ChameleonRole implements GrantedAuthority {
    private String role;
    private String subSys;

    public String getAuthority() {
        return role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getSubSys() {
        return subSys;
    }

    public void setSubSys(String subSys) {
        this.subSys = subSys;
    }

    @Override
    public String toString() {
        return "ChameleonRole{" +
                "role='" + role + '\'' +
                ", subSys='" + subSys + '\'' +
                '}';
    }
}
