package com.pb.pmv.envelope.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by it200190pmv on 10/28/15.
 */
@Controller
public class TicketsController {
    @RequestMapping(value = "/tickets", method = RequestMethod.GET)
    public ModelAndView todo(){
        ModelAndView model = new ModelAndView();
        model.setViewName("tickets");
        return model;
    }

}
