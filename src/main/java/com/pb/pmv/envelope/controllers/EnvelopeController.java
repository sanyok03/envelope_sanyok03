package com.pb.pmv.envelope.controllers;

import com.pb.pmv.envelope.commons.messages.Ticket;
import com.pb.pmv.envelope.service.EnvelopeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Обработка запросов которые перенаправляются в ПК Конверты
 *
 * Created by it200190pmv on 10/29/15.
 */
@RestController
@RequestMapping("/api")
public class EnvelopeController {
    private static final Logger LOGGER = LoggerFactory.getLogger(EnvelopeController.class);
    @Autowired
    EnvelopeService envelopeService;

    @RequestMapping("/ticket")
    public Ticket getTicket(@RequestParam(value="id", defaultValue="0") long id) {
        LOGGER.info("getTicket");
        return envelopeService.getTicket(id);
    }

    @RequestMapping("/tickets")
    public List<Ticket> getAllTickets() {
        LOGGER.info("getAllTickets");
        return envelopeService.getAllTickets();
    }
}
