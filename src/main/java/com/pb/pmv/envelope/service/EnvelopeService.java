package com.pb.pmv.envelope.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.pb.pmv.envelope.commons.messages.Ticket;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by it200190pmv on 10/29/15.
 */
@Service
public class EnvelopeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EnvelopeService.class);

    private static final List<Ticket> TICKET_LIST = new ArrayList<>();

    @PostConstruct
    private void initTickets(){
        TICKET_LIST.add(new Ticket(111,new Date(),1, "","NOVALIDATION"));
        TICKET_LIST.add(new Ticket(112,new Date(),12, "","ACCEPTED"));
        TICKET_LIST.add(new Ticket(113,new Date(),17, "","DEFECTED"));
        TICKET_LIST.add(new Ticket(114,new Date(),2126, "","REJECTED"));
        LOGGER.info("EnvelopeService is created");
    }

    public List<Ticket> getAllTickets(){
        return TICKET_LIST;
    }


    public Ticket getTicket(long id) {
        for (Ticket ticket : TICKET_LIST) {
            if(id==ticket.getId())
                return ticket;
        }
        return new Ticket();
    }
}
