<%--
  Created by IntelliJ IDEA.
  User: it200190pmv
  Date: 10/8/15
  Time: 9:19 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Bootstrap -->
    <spring:url value="/resources/core/css/hello.css" var="coreCss" />
    <spring:url value="/resources/core/css/vendors/bootstrap.min.css" var="bootstrapCss" />
    <link href="${bootstrapCss}" rel="stylesheet" />
    <link href="${coreCss}" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- ========= -->
    <!--    CSS    -->
    <!-- ========= -->
    <style type="text/css">
        #todoapp ul {
            list-style-type: none; /* Hides bullet points from todo list */
        }

        #todo-list input.edit {
            display: none; /* Hides input box*/
        }

        #todo-list .editing label {
            display: none; /* Hides label text when .editing*/
        }

        #todo-list .editing input.edit {
            display: inline; /* Shows input text box when .editing*/
        }

        #todo-list label {
            background-color: indianred;
        }

        #todo-list label.completed {
            background-color: green;
        }

        #todo-list input.edit {
            background-color: darkgrey;
        }
    </style>
</head>
<body>
<!-- ========= -->
<!-- Your HTML -->
<!-- ========= -->
<div class="container">
    <section id="todoapp">
        <header id="header">
            <h1>Todos</h1>
            <input id="new-todo" placeholder="What needs to be done?" autofocus>

            <div>
                <a href="#/">show all</a> |
                <a href="#/pending">show pending</a> |
                <a href="#/completed">show completed</a>
            </div>
        </header>
        <section id="main">
            <ul id="todo-list"></ul>
        </section>
    </section>
</div>

<!-- Templates -->
<spring:url value="/resources/core/js/templates/item-template.js" var="itemtemplateJs" />
<script type="text/template" id="item-template" src="${itemtemplateJs}">
    <%--<div class="view">--%>
        <%--<input class="toggle col-md-1" type="checkbox" <%= completed ? 'checked' : '' %>>--%>
        <%--<label class="col-md-8"><%-title %></label>--%>
        <%--<input class="edit col-md-8" value="<%- title %>">--%>
        <%--<button class="btn btn-warning destroy col-md-3">remove</button>--%>
    <%--</div>--%>
</script>

<spring:url value="/resources/core/js/hello.js" var="coreJs" />
<spring:url value="/resources/core/js/app.js" var="appJS" />
<spring:url value="/resources/core/js/vendors/backbone.localStorage.js" var="backbonelocalStorageJS" />
<spring:url value="/resources/core/js/vendors/jquery-2.1.4.js" var="jqueryJS" />
<spring:url value="/resources/core/js/vendors/underscore.js" var="underscoreJS" />
<spring:url value="/resources/core/js/vendors/backbone.js" var="backboneJS" />
<spring:url value="/resources/core/js/vendors/bootstrap.min.js" var="bootstrapJs" />

<script src="${coreJs}"></script>

<script src="${jqueryJS}"></script>
<script src="${underscoreJS}"></script>
<script src="${backboneJS}"></script>
<script src="${backbonelocalStorageJS}"></script>
<script src="${bootstrapJs}"></script>
<script src="${appJS}"></script>

</body>
</html>
