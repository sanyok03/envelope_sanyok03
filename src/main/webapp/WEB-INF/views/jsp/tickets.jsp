<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: it200190pmv
  Date: 10/28/15
  Time: 3:18 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en" ng-app="ticketsApp">
<head>
    <title>Tickets</title>
    <spring:url value="/resources/core/css/tickets.css" var="ticketsCss"/>
    <spring:url value="/resources/core/css/vendors/bootstrap.min.css" var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>
    <link href="${ticketsCss}" rel="stylesheet"/>
</head>
<body>
<!-- Static navbar -->
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#navbar"
                    aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">ВебКонверты v2.0</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#login">Login</a></li>
                <li><a href="#logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>


<div class="container" ng-controller="ticketsListCtrl">
    <div class="page-header">
        <h3>Количество Тикетов - {{model.tickets.length}}</h3>
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>ID тикета</th>
                <th>Создан</th>
                <th>Тип документа</th>
                <th>Описание</th>
                <th>Статус документа</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody ng-cloak>
            <tr ng-repeat="ticket in model.tickets">
                <td>{{ticket.id}}</td>
                <td>{{ticket.date}}</td>
                <td>{{ticket.type}}</td>
                <td>{{ticket.text}}</td>
                <td>
                    <span style="{{setColorByStatusinTicketRow(ticket.statys)}}">
                        {{ticket.statys}}
                    </span>
                </td>
                <td>Do something</td>
            </tr>
        </tbody>
    </table>
</div>

<spring:url value="/resources/core/js/vendors/angular.min.js" var="angularJs"/>
<spring:url value="/resources/core/js/tickets.js" var="ticketsjs"/>
<spring:url value="/resources/core/js/vendors/bootstrap.min.js" var="bootstrapJs"/>
<spring:url value="/resources/core/js/vendors/jquery-2.1.4.js" var="jqueryJS"/>
<script src="${angularJs}"></script>
<script src="${jqueryJS}"></script>
<script src="${ticketsjs}"></script>
<script src="${bootstrapJs}"></script>
</body>
</html>
