var model = {
    user: 'it200190pmv',
    //tickets: [
        //ID тикета	Создан	Тип документа	Описание	Статус документа	Действия
        //{ id: '1111111', date:'29-10-2015 15:00:00',type:2126, text:'Описание1', statys:'NOVALIDATION' },
        //{ id: '1111112', date:'29-10-2015 15:00:00',type:2127, text:'Описание2', statys:'ACCEPTED' },
        //{ id: '1111113', date:'29-10-2015 15:00:00',type:2128, text:'Описание3', statys:'DEFECTED' },
        //{ id: '1111114', date:'29-10-2015 15:00:00',type:2129, text:'Описание4', statys:'REJECTED' }
    //],
    statuses:['NOVALIDATION','ACCEPTED','DEFECTED','REJECTED']
};

// Модуль
var ticketsApp = angular.module('ticketsApp', []);

ticketsApp.constant("baseUrl", "http://10.1.196.206:8090")

// Контроллер
ticketsApp.controller("ticketsListCtrl", function ($scope, $http, baseUrl) {
    $scope.refresh = function(){
        $http.get(baseUrl+'/webnvlp/api/tickets').success(function(respounce){
                model.tickets= respounce;
                $scope.model = model;
        });
    };

    $scope.setColorByStatusinTicketRow = function(status){
        if(status.toUpperCase() === model.statuses[0]) return 'color:grey';
        else if(status.toUpperCase() === model.statuses[1]) return 'color:green';
        else if(status.toUpperCase() === model.statuses[2]) return 'color:red';
        else if(status.toUpperCase() === model.statuses[3]) return 'color:red';
        return 'color:green';
    };

    $scope.refresh();
});
