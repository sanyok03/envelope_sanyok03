/**
 * Created by it200190pmv on 10/8/15.
 */
<div class="view">
<input class="toggle col-md-1" type="checkbox" <%= completed ? 'checked' : '' %>>
<label class="col-md-8"><%-title %></label>
<input class="edit col-md-8" value="<%- title %>">
<button class="btn btn-warning destroy col-md-3">remove</button>
</div>
